/// <reference types='codeceptjs' />
type steps_file = typeof import('./steps_file.js');
type homePage = typeof import('./pages/homePage.js');
type loginPage = typeof import('./pages/loginPage.js');
type Data = import('./helpers/data_helper.js');
type PageObj = import('./helpers/pageobj_helper.js');

declare namespace CodeceptJS {
  interface SupportObject { I: I, current: any, homePage: homePage, loginPage: loginPage }
  interface Methods extends WebDriver, Data, PageObj {}
  interface I extends ReturnType<steps_file>, WithTranslation<Data>, WithTranslation<PageObj> {}
  namespace Translation {
    interface Actions {}
  }
}
