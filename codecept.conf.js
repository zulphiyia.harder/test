exports.config = {
  tests: './tests/*_test.js',
  pages: '/pages/',
  output: './output',
  helpers: {
    /*How we define what helper we will be using
    We can use WebDriver, Puppeteer, Playwright, etc.
    We can add different configurations to browsers as well as change browser type */
    WebDriver: {
      url: 'http://localhost',
      browser: 'chrome',
      desiredCapabilities: {
        chromeOptions: {
          args: ["--disable-features=SameSiteByDefaultCookies"]
        }
      },
      keepBrowserState: true,
      restart: false,
      windowSize: 'maximize'
    },
    Data: {
      require: './helpers/data_helper.js'
    },
    PageObj: {
      require: './helpers/pageobj_helper.js'
    },
  },
  //Store pages here
  include: {
    I: './steps_file.js',
    homePage: './pages/homePage.js',
    loginPage: './pages/loginPage.js',
    cartPage: './pages/cartPage.js',
    checkOutPage: './pages/checkOutPage.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'UI',
  plugins: {
    //Pauses a test on fail to allow user to look at test where item has failed as well open debugging console
    pauseOnFail: {
      enabled: true
    },
    //Retries steps that fail
    retryFailedStep: {
      enabled: true
    },
    //Stores screenshots in output folder at fails
    screenshotOnFail: {
      enabled: true
    },
    //Automatically generates reports for test runs based on output folder
    allure: {
      enabled: true,
      outputDir: "./output"
    },
    //Puts a slight delay on scripts to prevent automation of running faster than the application can handle
    autoDelay: {
      enabled: true
    },
    wdio: {
      enabled: true,
      services: ['selenium-standalone']
    }
  }
}