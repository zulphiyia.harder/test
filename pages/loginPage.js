const { I } = inject();

module.exports = {
    fields: {
        username: { xpath: "//input[@id = 'user-name']" },
        password: { xpath: "//input[@id = 'password']" }

    },

    buttons: {
        loginButton: { xpath: "//*[@id = 'login-button']" }
    },

    dropdowns: {

    },

    checkboxes: {

    },

    labels: {
        loginHeader: { xpath: "//div[@class = 'login_logo']" },
        errMsg: { xpath: "//h3[@data-test = 'error']" }
    },

    /*Allows for different roles with different accesses to login
    Declares data obj which reads from JSON file and retrieves the values we need based on key provided*/
    async roleLogin(role) {
        let data = await I.getData('loginDetails.json');
        let userId = null;

        switch ((role).toUpperCase()) {
            case 'STANDARDUSER':
                userId = data.standardUser;
                break;
            case 'LOCKEDOUTUSER':
                userId = data.lockedOutUser;
                break;
        }

        I.amOnPage(data.url);
        //'I.see' works as an assertion
        I.seeInCurrentUrl(data.url);
        await this.checkPage();

        I.fillField(this.fields.username, userId);
        I.fillField(this.fields.password, data.password);
        await I.takeScreenshot('loginPageFilled.png');

        I.click(this.buttons.loginButton);

    },

    async clearLoginFields() {
        I.clearField(this.fields.username);
        I.clearField(this.fields.password);

        I.seeInField(this.fields.username, '');
        I.seeInField(this.fields.password, '');
    },

    //Check for a proper load on the page - typically we check for stable elements
    async checkPage() {
        I.waitForElement(this.labels.loginHeader, 10);
        I.seeElement(this.labels.loginHeader);

        I.waitForElement(this.buttons.loginButton, 10);
        I.waitForClickable(this.buttons.loginButton, 10);
        I.seeElement(this.buttons.loginButton);

        await I.takeScreenshot('loginPageLoaded.png');                                                                                                                        
    },

    async checkError() {
        I.waitForElement(this.labels.errMsg, 10);
        I.seeElement(this.labels.errMsg);
        I.takeScreenshot('LoginError.png');
    }

}