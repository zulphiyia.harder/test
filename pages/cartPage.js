const { I } = inject();

module.exports = {
    fields: {

    },

    buttons: {
        continueShop: { xpath: "//button[contains(text(), 'Continue')]" },
        checkout: { xpath: "//button[contains(text(), 'Checkout')]" }
    },

    dropdowns: {

    },

    dropdownOptions: {

    },

    checkboxes: {

    },

    labels: {
        itemName(item) {
            return { xpath: "//*[text()='" + item + "']" }
        },

        itemPrice(item, price) {
            return { xpath: "//*[contains(text(), '" + item + "')]//following::div[@class='inventory_item_price' and text()='" + price + "']" }
        },

        itemsInCart: { xpath: "//*[@class='inventory_item_name']" }
    },

    async validateItemAddedToCart(item) {
        let itemDesc = await I.getData('shoppingItems.json');
        let itemsOnPage = await I.grabTextFromAll(this.labels.itemsInCart);
        let desiredItem = null;

        if (item === 'backpack') {
            desiredItem = itemsOnPage.find(element => element === 'Sauce Labs Backpack')
            I.assertEqual(itemDesc.backpack.name, desiredItem);
            I.seeTextEquals(itemDesc.backpack.priceOnSite, this.labels.itemPrice(itemDesc.backpack.name, itemDesc.backpack.price));
        }
        if (item === 'bike') {
            desiredItem = itemsOnPage.find(element => element === 'Sauce Labs Bike Light')
            I.assertEqual(itemDesc.bike.name, desiredItem);
            I.seeTextEquals(itemDesc.bike.priceOnSite, this.labels.itemPrice(itemDesc.bike.name, itemDesc.bike.price));
        }
        if (item === 'bolt') {
            desiredItem = itemsOnPage.find(element => element === 'Sauce Labs Bolt T-Shirt')
            I.assertEqual(itemDesc.bolt.name, desiredItem);
            I.seeTextEquals(itemDesc.bolt.priceOnSite, this.labels.itemPrice(itemDesc.bolt.name, itemDesc.bolt.price));
        }
        if (item === 'jacket') {
            desiredItem = itemsOnPage.find(element => element === 'Sauce Labs Fleece Jacket')
            I.assertEqual(itemDesc.jacket.name, desiredItem);
            I.seeTextEquals(itemDesc.jacket.priceOnSite, this.labels.itemPrice(itemDesc.jacket.name, itemDesc.jacket.price));
        }
        if (item === 'onesie') {
            desiredItem = itemsOnPage.find(element => element === 'Sauce Labs Onesie')
            I.assertEqual(itemDesc.onesie.name, desiredItem);
            I.seeTextEquals(itemDesc.onesie.priceOnSite, this.labels.itemPrice(itemDesc.onesie.name, itemDesc.onesie.price));
        }
        if (item === 'red') {
            desiredItem = itemsOnPage.find(element => element === 'Test.allTheThings() T-Shirt (Red)')
            I.assertEqual(itemDesc.red.name, desiredItem);
            I.seeTextEquals(itemDesc.red.priceOnSite, this.labels.itemPrice(itemDesc.red.name, itemDesc.red.price));
        }
    },

    async goToHomePage() {
        I.waitForClickable(this.buttons.continueShop, 10);
        I.seeElement(this.buttons.continueShop);
        I.click(this.buttons.continueShop);
    },

    async goToCheckout() {
        I.waitForClickable(this.buttons.checkout, 10);
        I.seeElement(this.buttons.checkout);
        I.click(this.buttons.checkout);
    }

}