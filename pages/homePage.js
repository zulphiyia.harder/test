const cartPage = require("./cartPage");
const loginPage = require("./loginPage");

const { I } = inject();

module.exports = {
    fields: {

    },

    buttons: {
        sideBar: { xpath: "//button[contains(@id, 'menu-btn')]"},
  
        //available values: backpack, bike, bolt, jacket, onesie, red
        addToCartItem(item){
            return { xpath: "//*[contains(@id, 'add-to-cart') and contains(@id, '"+item+"')]"}
        },

        removeAddedItem(item){
            return { xpath: "//*[contains(@id, 'remove') and contains(@id, '"+item+"')]"}
        },

        cartPage: { xpath: "//*[@class='shopping_cart_link']"}
    },

    dropdowns: {
        filter: { xpath: "//*[class = 'product_sort_container']"}
    },

    dropdownOptions: {
        //available values: 'az', 'za, 'lohi', 'hilo'
        filterOption(filterValue){
            return { xpath: "//option[@value='"+filterValue+"']"}
        },
        //available values: 'logout', 'about', 'inventory', 'reset'
        sideBarOptions(sideOptions){
            return { xpath: "//a[@id='"+sideOptions+"_sidebar_link']"}
        }
    },

    checkboxes: {

    },

    labels: {
        webHeader: { xpath: "//div[@class = 'app_logo']" },
        productHeader: { xpath: "//*[contains(text(), 'Products')]"}
    },

    async checkPage(){
        I.waitForElement(this.labels.webHeader, 5);
        I.seeElement(this.labels.webHeader);

        I.waitForElement(this.labels.productHeader, 10);
        I.seeElement(this.labels.productHeader);
    },

    async filterDropDown(){
    },

    async addItemToCart(item){
        I.waitForClickable(this.buttons.addToCartItem(item), 10);
        I.seeElement(this.buttons.addToCartItem(item));
        I.click(this.buttons.addToCartItem(item));
        
        I.seeElement(this.buttons.removeAddedItem(item));

        this.goToCart(item);

        await cartPage.validateItemAddedToCart(item);
    },

    async goToCart(item){
        I.waitForClickable(this.buttons.cartPage, 10);
        I.click(this.buttons.cartPage);
    },

    async logOut(){
        I.dontSeeElement(this.dropdownOptions.sideBarOptions('logout'));

        I.waitForElement(this.buttons.sideBar, 10);
        I.waitForClickable(this.buttons.sideBar, 5);
        I.seeElement(this.buttons.sideBar);

        I.click(this.buttons.sideBar);
        I.waitForElement(this.dropdownOptions.sideBarOptions('logout'), 10);
        I.seeElement(this.dropdownOptions.sideBarOptions('logout'));
        I.click(this.dropdownOptions.sideBarOptions('logout'));

        await loginPage.checkPage();
    }
}
