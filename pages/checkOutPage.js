const { checkPage } = require("./homePage");

const { I } = inject();

module.exports = {
    fields: {
        firstName: { xpath: "//input[@id = 'first-name']" },
        lastName: { xpath: "//input[@id = 'last-name']" },
        zipcode: { xpath: "//input[@id = 'postal-code']" }
    },

    buttons: {
        continue: { xpath: "//*[@id = 'continue']" },
        cancel: { xpath: "//*[@id = 'cancel']" },
        finishCheckout: { xpath: "//*[@id = 'finish']" }
    },

    dropdowns: {

    },

    dropdownOptions: {
        
    },

    checkboxes: {

    },

    labels: {
        checkoutComplete: { xpath: "//span[contains(text(), 'Complete')]"}
    },

    async checkPage() {
        I.waitForElement(this.fields.firstName, 10);
        I.seeElement(this.fields.firstName);
    },

    async fillShippingInfo() {
        let userDetails = await I.getData('loginDetails.json');
        I.fillField(this.fields.firstName, userDetails.usersFirstName);
        I.fillField(this.fields.lastName, userDetails.usersLastName);
        I.fillField(this.fields.zipcode, userDetails.usersZipcode);

        I.seeInField(this.fields.firstName, userDetails.usersFirstName);
        I.seeInField(this.fields.lastName, userDetails.usersLastName);
        I.seeInField(this.fields.zipcode, userDetails.usersZipcode);

        I.click(this.buttons.continue);
    },

    async checkOut() {
        I.waitForClickable(this.buttons.finishCheckout, 10);
        I.click(this.buttons.finishCheckout);

        I.waitForElement(this.labels.checkoutComplete, 10);
        I.seeElement(this.labels.checkoutComplete);
    },



}