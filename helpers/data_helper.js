const Helper = require('@codeceptjs/helper');
var fs = require('fs');
const chai = require('chai');
const assert = chai.assert;

class Data extends Helper {
  constructor(config) {
    super(config);
  }

  async getData(dataFileName) {

    //gets env to check corresponding env data
    let envirData = JSON.parse(fs.readFileSync(process.cwd() + "/data/environment.json").toString());
    let filePath;

    //build the file path with envirData
    if (envirData.env) {
      filePath = 'data/' + envirData.env + '/' + dataFileName;

      if (!fs.existsSync(filePath)) {
        filePath = 'data/common/' + dataFileName;
      }

    } else {
      console.log('Environment not specified...');
    }
    //read the file and return data
    var data = fs.readFileSync(process.cwd() + '/' + filePath).toString();

    return JSON.parse(data);

  }

  async sendData(dataFileName, id) {
    //gets env to check corresponding env data
    let envirData = JSON.parse(fs.readFileSync(process.cwd() + "/data/environment.json").toString());
    let filePath;

    //build the file path with envirData
    if (envirData.env) {
      filePath = 'data/' + envirData.env + '/' + dataFileName;

      if (!fs.existsSync(filePath)) {
        filePath = 'data/common/' + dataFileName;
      }

    } else {
      console.log('Environment not specified...');
    }

    var data = fs.writeFileSync(process.cwd() + '/' + filePath, id);
  }

  async modifyJsonFile(dataFileName, key, value) {

    let envirData = JSON.parse(fs.readFileSync(process.cwd() + "/data/environment.json").toString());
    let filePath;

    //build the file path with envirData
    if (envirData.env) {
      filePath = 'data/' + envirData.env + '/' + dataFileName;

      if (!fs.existsSync(filePath)) {
        filePath = 'data/common/' + dataFileName;
      }

    } else {
      console.log('Environment not specified...');
    }

    //Read the Json
    let data = fs.readFileSync(process.cwd() + '/' + filePath).toString();
    let jdata = JSON.parse(data);
    //Get the key and value from JSON
    jdata[key] = value;
    let dataPath = process.cwd() + '/' + filePath;
    //Replace the key with new value
    fs.writeFileSync(dataPath, JSON.stringify(jdata));

  }

  async getRandomNum(min, max){
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  async getCurrentDate(){
    let today = new Date();
    let date = ('0' + today.getDate()).slice(-2);
    let month = ('0' + (today.getMonth() + 1)).slice(-2);
    let year = today.getFullYear();

    let fullDate = month + '/' + date + '/' + year;
    console.log(fullDate);
    return fullDate;
  }

}

module.exports = Data;
