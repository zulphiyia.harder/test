const Helper = require('@codeceptjs/helper');
const { assert } = require('chai');
var fs = require('fs');

class PageObj extends Helper {

  async assertEqual(actual, expected) {
    assert.equal(actual, expected, 'Assertion Failed => actual result: ' + actual + ' vs' + expected);
  }

  async assertNotNull(str) {
    assert.isNotNull(str, 'Assertion Failed String is NULL => Actual Result: ' + str);
  }

  async assertArr(arr){
    assert.isArray(arr, 'Assertion Failed expected Array => Actual Result: ' + arr);
  }

  async takeScreenshot(name) {
    this.helpers['WebDriver'].saveScreenshot(name, false);
    fs.renameSync('./output/' + name, './output/screenshots/' + name);
  }
}

module.exports = PageObj;
