Feature('Validate Login Page allows successful Login');

Scenario('User logs into website successfully and logs out', async ({ I, loginPage, homePage }) => {
    //We use await as we execute our methods asynchronously
    await loginPage.roleLogin('standardUser');
    await homePage.checkPage();
    await homePage.logOut();
    await loginPage.checkPage();
}).tag('@smoke');
 
Scenario('User attempts to login as locked out user', async ({ I, loginPage }) => {
    await loginPage.roleLogin('lockedoutuser');
    await loginPage.checkError();
    await loginPage.clearLoginFields();
}).tag('@smoke');
