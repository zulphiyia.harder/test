Feature('User purchases items from SauceDemo.com')

Scenario('User logs into website successfully', async ({ I, loginPage, homePage }) => {
    await loginPage.roleLogin('standardUser');
    await homePage.checkPage();
}).tag('@regression');


Scenario('User adds items to their cart that they want to purchase', async ({ I, homePage, cartPage }) => {
    await homePage.addItemToCart('backpack');
    await cartPage.goToHomePage();
    await homePage.checkPage();
    
    await homePage.addItemToCart('bike');
    await cartPage.goToHomePage();
    await homePage.checkPage();

    await homePage.addItemToCart('bolt');
    await cartPage.goToHomePage();
    await homePage.checkPage(); 

    await homePage.addItemToCart('jacket');
    await cartPage.goToHomePage();
    await homePage.checkPage();
    
    await homePage.addItemToCart('onesie');
    await cartPage.goToHomePage();
    await homePage.checkPage();

    await homePage.addItemToCart('red');

    await cartPage.goToCheckout();
}).tag('@regression');

Scenario('User fills in information and finishes shopping', async ({ I, homePage, checkOutPage  }) => {
    await checkOutPage.checkPage();
    await checkOutPage.fillShippingInfo();
    await checkOutPage.checkOut();

    await homePage.logOut();
}).tag('@regression');

